var img_senador1;
var img_senador2;
var img_senador3;
var llega_final_img1;
var llega_final_img2;
var llega_final_img3;
window.onload = function(){
	//seleccionar imágenes
	var senador1 = document.getElementById("senador1");
	var senador2 = document.getElementById("senador2");
	var senador3 = document.getElementById("senador3");
	var cab = document.getElementById("cab");

	//posiciones iniciales
	img_senador1 = 0;
	img_senador2 = 0;
	img_senador3 = 0;
	llega_final_img1=0;
	llega_final_img2=0;
	llega_final_img3=0;
}

function mover() {
	cab.style.display="none";
	//movimiento senador1
	if(img_senador1 <= 80 && llega_final_img1==0) {
		img_senador1 = img_senador1 + 2;
		senador1.style.left = img_senador1 + "%";
		if(img_senador1==80)
		{
			llega_final_img1=1;
		}
	}
	else if(llega_final_img1==1 && img_senador1>=0){
		img_senador1 = img_senador1 - 2;
		senador1.style.left = img_senador1 + "%";
		if(img_senador1==0)
		{
			llega_final_img1=0;
		}
	}

	//movimiento senador2
	if(img_senador2 <= 70 && llega_final_img2==0) {
		img_senador2 = img_senador2 + 2;
		senador2.style.top = img_senador2 + "%";
		if(img_senador2==70)
		{
			llega_final_img2=1;
		}
	}
	else if(llega_final_img2==1 && img_senador2>=0){
		img_senador2 = img_senador2 - 2;
		senador2.style.top = img_senador2 + "%";
		if(img_senador2==0)
		{
			llega_final_img2=0;
		}
	}

	//movimiento senador3
	if(img_senador3<=70 && llega_final_img3==0)
	{
		img_senador3=img_senador3+2;
		senador3.style.top=img_senador3 + "%";
		senador3.style.left=img_senador3 +"%";
		if(img_senador3==70)
		{
			console.log("llego img3 a 80");
			llega_final_img3=1;
		}
	}
	else if (llega_final_img3==1 && img_senador3>=0){
		img_senador3=img_senador3-2;
		senador3.style.top=img_senador3 + "%";
		senador3.style.left=img_senador3 +"%";
		if(img_senador3==0)
		{
			llega_final_img3=0;
		}
	}
}
